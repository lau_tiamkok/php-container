<?php 
function __autoload($classname) 
{
    $namespace = "\\";
    $parts = explode($namespace, $classname);
    
    // Set the class file name.
    $filename = "autoload/".end($parts).'.php';
    //var_dump($filename);
    
    if (is_readable($filename)) 
    {
        require_once $filename;
    }
}

$dice = new \Dice\Dice;
$Foo = $dice->create('Namie\Foo');
//var_dump($Foo);

echo '==========================================================<br/>';

// Host used to access Database.
define('DB_HOST', 'localhost');

// Username used to access Database.
define('DB_USER', 'root');

// Password for the username.
define('DB_PASS', 'tklau');

// Name of your databse.
define('DB_NAME', 'cms_master'); 

// Data source name.
define('DSN', 'mysql:host='.DB_HOST.';dbname='.DB_NAME);

//create a rule to apply to shared object
$rule = new \Dice\Rule;
$rule->shared = true;

//Database will be constructed by the container with these variables supplied to Database::__construct
$rule->constructParams = [DSN,DB_USER,DB_PASS];

//Apply the rule to instances of Database
$dice->addRule('Namie\Database', $rule);

//Now any time Database is requested from Dice, the same instance will be returned
$pdo = $dice->create('Namie\Database');
//$pdo2 = $dice->create('\Namie\Database');
//var_dump($pdo === $pdo2); //TRUE

//And any class which asks for an instance of Database will be given the same instance:
class MyClass {
    public $pdo;
    public function __construct(\Namie\Database $pdo) {
        $this->pdo = $pdo;
    }
}

// Make connection.
//$pdo->connect();
// or:
$pdo();
//var_dump($pdo);

$myobj = $dice->create('MyClass');
var_dump($myobj->pdo);
//var_dump($myobj);


$Article = $dice->create('Namie\Article');
var_dump($myobj->pdo === $Article->Database); //TRUE
//var_dump($Article->getRow());

echo '==========================================================<br/>';

class A {
    public function __construct(B $b) {
    }
    
    public function method1(E $e, $foo) {
        $this->e = $e;
        $this->message = $foo;
    }
    
    public function method2() {
        return "Method2 called\n";
    }
}

class B {
    private $c,$d;

    public function __construct(C $c, D $d) {
        $this->c = $c;
        $this->d = $d;
    }
}

class C {

}

class D {
    private $e;
    
    public function __construct(E $e) {
        $this->e = $e;
    }
}

class E {
    public $message = 'Hello E';
}


$rule = new \Dice\Rule;
$rule->call['1'] = ['method1', ['too']];
//$rule->call['3'] = ['method2', []];

$dice->addRule('A', $rule);
$a = $dice->create('A');

$rule2 = clone $dice->getRule('A');
//var_dump($rule2);
$rule2->call['3'] = ['method2', ['foo']];

$rule3 = clone $dice->getRule('A');
var_dump($rule3);


var_dump(get_class_methods($a));
var_dump($a);
?>