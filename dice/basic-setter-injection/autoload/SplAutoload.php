<?php
/**
 * Auto load classes.
 */
class SplAutoload
{
    /**
     * Set the property.
     */
    public $directories;

    /**
     * Receive the supplied data.
     * @string $directories
     * @array $recursive default: models
     */
    public function __construct() 
    {
    }
    
    public function fetch($directories) 
    {
        // Store the data into the property.
        $this->directories = $directories;

        // When using spl_autoload_register() with class methods, it might seem that it can use only public methods, 
        // though it can use private/protected methods as well, if registered from inside the class:
        spl_autoload_register(array($this,'getClass'));
    }

    private function getClass($className)
    {
        if(is_array($this->directories)): $mainDirectories =  $this->directories;
        else: $mainDirectories =  array($this->directories); endif;
            
        // Set other vars and arrays.
        $subDirectories = [];
        //print_r($mainDirectories);
        
        $namespace = "\\";
        $isNamespace = false;
        
        // When you use namespace in a class, you get something like this when you auto load that class \foo\tidy.
        // So use explode to split the string and then get the last item in the exloded array.
        $parts = explode($namespace, $className);

        // Check if the current class is a namespace class or not.
        if(strpos($className, $namespace) !== false) 
        {
            $isNamespace = true;
        }
        
        // Set the class file name.
        $filename = end($parts).'.php';
        
        // List any sub dirs in the main dirs above and store them in an array.
        foreach($mainDirectories as $mainDirectory)
        {
            // RecursiveIteratorIterator is too slow on each browser request.
            /*
            $iterator = new RecursiveIteratorIterator
            (
                new RecursiveDirectoryIterator(WEBSITE_DOCROOT.$mainDirectory), // Must use absolute path to get the files when ajax is used.
                RecursiveIteratorIterator::SELF_FIRST
            );

            foreach ($iterator as $fileObject) 
            {
                if ($fileObject->isDir()) 
                {
                    // Replace any backslash to '/'.
                    $pathnameReplace = str_replace('\\', '/', $fileObject->getPathname());
                    //print_r($pathnameReplace);

                    // Explode the folder path.
                    $array = explode("/",$pathnameReplace);

                    // Get the actual folder.
                    $folder = end($array);
                    //print_r($folder);

                    // Stop proccessing if the folder is a dot or double dots.
                    if($folder === '.' || $folder === '..') {continue;} 
                    //var_dump($fileObject->getPathname());

                    // Must trim off the WEBSITE_DOCROOT. 
                    $subDirectories2[] = preg_replace('~.*?(?=core|local)~i', '', str_replace('\\', '/', $fileObject->getPathname())) .'/';
                }
            }
             * 
             */
            
            // Must use absolute path to get the files when ajax is used.
            foreach(glob(WEBSITE_DOCROOT.$mainDirectory.'*', GLOB_ONLYDIR) as $dir) 
            {
                // Must trim off the WEBSITE_DOCROOT. 
                $subDirectories[] = preg_replace('~.*?(?=core|local)~i', '', str_replace('\\', '/', $dir)) .'/';
                //$dir = str_replace($mainDirectory, '', $dir);
                //$subDirectories[] = $dir.'/';
            }
        }
        
        // Mearge the main dirs with any sub dirs in them.
        $mergedDirectories = array_merge($mainDirectories,$subDirectories);
        //print_r($mergedDirectories);

        // Loop the merge array and include the classes in them.
        foreach($mergedDirectories as $mainDirectory)
        {
            if(file_exists(WEBSITE_DOCROOT.$mainDirectory.$filename))
            {
                // There is no need to use include/require_once. Autoload is a fallback when the system can't find the class you are instantiating. 
                // If you've already included it once via an autoload then the system knows about it and won't run your autoload method again anyway. 
                // So, just use the regular include/require - they are faster.
                include_once WEBSITE_DOCROOT.$mainDirectory.$filename;

                // Check if the class has existed.
                if($isNamespace === false) if (class_exists($className)) break;
            }
        }
    }

}

/**
 *  Below are the original ideas
 */

/**
 * @option 1:
 *

function autoloadMultipleDirectory($className) 
{
    $isCms = preg_match('~.*?(?=cms)~i', $_SERVER['REQUEST_URI']) > 0 ? true : false;
    
    if($isCms === false) 
    {
        // List all the class directories in the array.
        $mainDirectories = array(
            'local/container/',
            'local/controller/',
            'local/model/',
            'local/helper/',
            'local/ext/',
            'core/container/',
            'core/controller/', 
            'core/model/',
            'core/helper/',
            'core/ext/'
        ); 
    }
    else 
    {
        // List all the class directories in the array.
        $mainDirectories = array(
            'core/container/',
            'core/controller/', 
            'core/model/',
            'core/helper/',
            'core/ext/'
        ); 
    }
    
    // Set other vars and arrays.
    $subDirectories = array();

    // When you use namespace in a class, you get something like this when you auto load that class \foo\tidy.
    // So use explode to split the string and then get the last item in the exloded array.
    $parts = explode('\\', $className);

    // Set the class file name.
    $file_name = end($parts).'.php';
    // $file_name = strtolower(end($parts)).'.php';
    
    //print_r($mainDirectories);

    // List any sub dirs in the main dirs above and store them in an array.
    foreach($mainDirectories as $mainDirectory)
    {
        $iterator = new RecursiveIteratorIterator
        (
            new RecursiveDirectoryIterator(WEBSITE_DOCROOT.$mainDirectory), // Must use absolute path to get the files when ajax is used.
            RecursiveIteratorIterator::SELF_FIRST
        );
        
        foreach ($iterator as $fileObject) 
        {
            if ($fileObject->isDir()) 
            {
                // Replace any backslash to '/'.
                $pathnameReplace = str_replace('\\', '/', $fileObject->getPathname());
                //print_r($pathnameReplace);
                
                // Explode the folder path.
                $array = explode("/",$pathnameReplace);

                // Get the actual folder.
                $folder = end($array);
                //print_r($folder);

                // Stop proccessing if the folder is a dot or double dots.
                if($folder === '.' || $folder === '..') {continue;} 
                //var_dump($fileObject->getPathname());

                // Must trim off the WEBSITE_DOCROOT. 
                $subDirectories[] = preg_replace('~.*?(?=core|local)~i', '', str_replace('\\', '/', $fileObject->getPathname())) .'/';
            }
        }
    }
    //echo ("new count/ ");
    //print_r($subDirectories);
    
    // Mearge the main dirs with any sub dirs in them.
    $mergedDirectories = array_merge($mainDirectories,$subDirectories);
    //print_r($mergedDirectories);
    
    // Loop the merge array and include the classes in them.
    foreach($mergedDirectories as $mainDirectory)
    {
        if(file_exists(WEBSITE_DOCROOT.$mainDirectory.$file_name))
        {
            // There is no need to use include/require_once. Autoload is a fallback when the system can't find the class you are instantiating. 
            // If you've already included it once via an autoload then the system knows about it and won't run your autoload method again anyway. 
            // So, just use the regular include/require - they are faster.
            include WEBSITE_DOCROOT.$mainDirectory.$file_name;
            
            // Check if the class has existed.
            if (class_exists($className)) break;
        } 
    }
}

*/

/**
 * @option 2:
 *

function autoload_class_multiple_directory($className) 
{
        // List all the class directories in the array.
        $array_directories = array(
                'core/controllers/', 
                'core/models/',
                'local/controllers/',
                'local/models/'
        );

        // Loop the array.
        foreach($array_directories as $mainDirectory)
        {
                // %s%s and %s are placeholders for a string-value in sprintf. There are more, you can read more about it in 
                // php.net/manual/en/function.sprintf.php. I normally use sprintf instead of merging variable together inside 
                // a string, because it can be more fine-grained controlled and you see all variables after the formatting string 
                // as a parameter of it's own.
                $file = sprintf('%s%s/class_%s.php', WEBSITE_DOCROOT, $mainDirectory, $className);
                if(is_file($file)) 
                {
                        // There is no need to use include/require_once. Autoload is a fallback when the system can't find the class you are instantiating. 
                        // If you've already included it once via an autoload then the system knows about it and won't run your autoload method again anyway. 
                        // So, just use the regular include/require - they are faster.
                        include_once $file;
                } 
        }
}
*/

/*
 * @option 3:
 *
class base_autoloader {

        private $type = null;
        private $array_directories = array(
                'core/controllers/', 
                'core/models/',
                'local/controllers/',
                'local/models/'
        );

        public function __construct($type)
        {
                $this->type = $type;
        }

        public function autoload($className) {

                // When you use namespace in a class, you get something like this when you auto load that class \foo\tidy.
                // So use explode to split the string and then get the last item in the exloded array.
                $parts = explode('\\', $className);
                //print_r($parts);

                // Set the class file name.
                if($this->type == 'interface') $file_name = 'interface_'.strtolower(end($parts)).'.php';
                        else $file_name = 'class_'.strtolower($className).'.php';

                // Loop the array.
                foreach($this->array_directories as $mainDirectory)
                {
                        if(file_exists(WEBSITE_DOCROOT.$mainDirectory.$file_name)) 
                        {
                                // There is no need to use include/require_once. Autoload is a fallback when the system can't find the class you are instantiating. 
                                // If you've already included it once via an autoload then the system knows about it and won't run your autoload method again anyway. 
                                // So, just use the regular include/require - they are faster.
                                include WEBSITE_DOCROOT.$mainDirectory.$file_name;
                        } 
                }
        }
}

// If your autoload function is a class method, you can call spl_autoload_register with an array specifying the class and the method to run.
// @reference: http://php.net/manual/en/function.spl-autoload-register.php
$classes = new CoreModel_autoloader('class');
$interfaces = new CoreModel_autoloader('interface');
*/

// Register all the classes.
//spl_autoload_register('autoloadMultipleDirectory');
//spl_autoload_register(array($classes, 'autoload'));
//spl_autoload_register(array($interfaces, 'autoload'));

?>