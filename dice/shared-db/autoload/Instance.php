<?php 
/* @description		Dice - A minimal Dependency Injection Container for PHP				*  
 * @author			Tom Butler tom@r.je													*
 * @copyright		2012-2014 Tom Butler <tom@r.je> | http://r.je/dice.html				*
 * @license			http://www.opensource.org/licenses/bsd-license.php  BSD License 	*
 * @version			1.3.1																*/
namespace Dice;

class Instance {
	public $name;
	public function __construct($instance) {
		$this->name = $instance;
	}
}
