<?php
/**
 * Auto include functions.
 */
class FileAutoload
{
    /**
     * Set the property.
     */
    public $directories;

    /**
     * Receive the supplied data.
     * @string $directories
     * @array $recursive default: models
     */
    public function __construct() 
    {
    }
    
    public function fetch($directories) 
    {
        // Store the data into the property.
        $this->directories = $directories;
        
        // Execute the function.
        $this->getFile();
    }

    private function getFile()
    {
        if (is_array($this->directories)): $mainDirectories =  $this->directories;
        else: $mainDirectories =  array($this->directories); endif;
            
        // Set other vars and arrays.
        $subDirectories = [];
        $files = [];
        //print_r($mainDirectories);
        
        // List any sub dirs in the main dirs above and store them in an array.
        foreach($mainDirectories as $mainDirectory)
        {
            // Must use absolute path to get the files when ajax is used.
            foreach(glob(WEBSITE_DOCROOT.$mainDirectory.'*', GLOB_ONLYDIR) as $dir) 
            {
                // Must trim off the WEBSITE_DOCROOT. 
                $subDirectories[] = preg_replace('~.*?(?=core|local)~i', '', str_replace('\\', '/', $dir)) .'/';
                //$dir = str_replace($mainDirectory, '', $dir);
                //$subDirectories[] = $dir.'/';
            }
        }

        // Mearge the main dirs with any sub dirs in them.
        $mergedDirectories = array_merge($mainDirectories,$subDirectories);
        //print_r($mergedDirectories);

        // Loop the merge array and include the classes in them.
        foreach($mergedDirectories as $mainDirectory)
        {
            // List all the php files inside the folder.
            $files[] = glob(WEBSITE_DOCROOT.$mainDirectory.'*.php');
        }
        //print_r(call_user_func_array('array_merge', $files));

        // Flatten the multiple array.
        $array_flatten = call_user_func_array('array_merge', $files);

        // Loop and include the files.
        foreach($array_flatten as $file)
        {
            include $file;
        }
    }

}

/**
 *  Below are the original ideas
 * 

// Recursive auto include method.
function autoIncludeFunction() 
{
    $isCms = preg_match('~.*?(?=cms)~i', $_SERVER['REQUEST_URI']) > 0 ? true : false;
    
    if($isCms === false) 
    {
       // List all the class directories in the array.
        $mainDirectories = array(
            'core/function/', 
            'local/function/'
        ); 
    }
    else
    {
        // List all the class directories in the array.
        $mainDirectories = array(
            'core/function/'
        );
    }
    
    
    // Set other vars and arrays.
    $subDirectories = array();
    $files = array();
    
    // List any sub dirs in the main dirs above and store them in an array.
    foreach($mainDirectories as $mainDirectory)
    {
        $iterator = new RecursiveIteratorIterator
        (
            new RecursiveDirectoryIterator(WEBSITE_DOCROOT.$mainDirectory), // Must use absolute path to get the files when ajax is used.
            RecursiveIteratorIterator::SELF_FIRST
        );
        
        foreach ($iterator as $fileObject) 
        {
            if ($fileObject->isDir()) 
            {
                // Replace any backslash to '/'.
                $pathnameReplace = str_replace('\\', '/', $fileObject->getPathname());

                // Explode the folder path.
                $array = explode("/",$pathnameReplace);

                // Get the actual folder.
                $folder = end($array);
                //var_dump($folder);

                // Stop proccessing if the folder is a dot or double dots.
                if($folder === '.' || $folder === '..') {continue;} 
                //var_dump($fileObject->getPathname());

                // Must trim off the WEBSITE_DOCROOT. 
                $subDirectories[] = preg_replace('~.*?(?=core|local)~i', '', str_replace('\\', '/', $fileObject->getPathname())) .'/';
            }
        }
    }
    
    // Mearge the main dirs with any sub dirs in them.
    $mergedDirectories = array_merge($mainDirectories,$subDirectories);
    //print_r($mergedDirectories);
    
    // Loop the merge array and include the classes in them.
    foreach($mergedDirectories as $mainDirectory)
    {
        // List all the php files inside the folder.
        $files[] = glob(WEBSITE_DOCROOT.$mainDirectory.'*.php');
    }
    //print_r(call_user_func_array('array_merge', $files));
    
    // Flatten the multiple array.
    $array_flatten = call_user_func_array('array_merge', $files);
    
    // Loop and include the files.
    foreach($array_flatten as $file)
    {
        include $file;
    }
}

// Execute the function.
autoIncludeFunction();

 */

?>