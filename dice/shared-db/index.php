<?php
include "snippets.php";
include "Database.php";

// Host used to access Database.
define('DB_HOST', 'localhost');

// Username used to access Database.
define('DB_USER', 'root');

// Password for the username.
define('DB_PASS', 'tklau');

// Name of your databse.
define('DB_NAME', 'cms_master'); 

// Data source name.
define('DSN', 'mysql:host='.DB_HOST.';dbname='.DB_NAME);

// Load Database class only
// Instance of Database.
//$Database = new Database(DSN,DB_USER,DB_PASS);

// Make connection.
//$Database->connect();

//var_dump($Database);

// Load subsequent classes
function __autoload($classname) 
{
    $namespace = "\\";
    $parts = explode($namespace, $classname);
    
    // Set the class file name.
    $filename = "autoload/".end($parts).'.php';
    //var_dump($filename);
    
    if (is_readable($filename)) 
    {
        require_once $filename;
    }
}


// Load Dice
// Instance of Dice.
$Dice = new \Dice\Dice;

//create a rule to apply to shared object
$Rule = new \Dice\Rule;
$Rule->shared = true;

//Database will be constructed by the container with these variables supplied to Database::__construct
$Rule->constructParams = [DSN,DB_USER,DB_PASS];

//Apply the rule to instances of Database
$Dice->addRule('MyName\Database', $Rule);

var_dump($Dice);

//Now any time Database is requested from Dice, the same instance will be returned
$Database = $Dice->create('MyName\Database');

var_dump($Dice);

//And any class which asks for an instance of Database will be given the same instance:
class MyClass {
    public $pdo;
    public function __construct(MyName\Database $pdo) {
        $this->pdo = $pdo;
    }
}

$myobj = $Dice->create('MyClass');
var_dump($myobj->pdo);
var_dump($myobj->pdo  === $Database);


?>