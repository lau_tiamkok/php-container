<?php

require_once __DIR__ . '/vendor/autoload.php';

$Dice = new \Dice\Dice;

/*
//create a rule to apply to shared object
$Rule = new \Dice\Rule;
$Rule->shared = true;

//PdoAdapter will be constructed by the container with these variables supplied to PdoAdapter::__construct
$Rule->constructParams = [
    $dsn = 'mysql:host=localhost;dbname=phpunit',
    $username = 'root',
    $password = 'o@bit837'
];

//Apply the rule to instances of PdoAdapter
$Dice->addRule('Foo\Adapter\PdoAdapter', $Rule);

//Now any time PdoAdapter is requested from Dice, the same instance will be returned
$PdoAdapter = $Dice->create('Foo\Adapter\PdoAdapter');

// Make connection.
$PdoAdapter->connect();
*/

$rule = [
    'constructParams' => [
        $dsn = 'mysql:host=localhost;dbname=phpunit',
        $username = 'root',
        $password = 'o@bit837'
    ],
    'shared' => true
];

//Apply the rule to instances of PdoAdapter
$Dice->addRule('Foo\Adapter\PdoAdapter', $rule);

//Now any time PdoAdapter is requested from Dice, the same instance will be returned
$PdoAdapter = $Dice->create('Foo\Adapter\PdoAdapter');

// Make connection.
$PdoAdapter->connect();

$Article = $Dice->create('Foo\Article');

//Now any time PDO is requested from Dice, the same instance will be returned
$pdo1 = $Dice->create('Foo\Adapter\PdoAdapter');
$pdo2 = $Dice->create('Foo\Adapter\PdoAdapter');
var_dump($pdo1 === $pdo2); //TRUE

//And any class which asks for an instance of PDO will be given the same instance:
class MyClass {
    public $pdo;
    public function __construct(Foo\Adapter\PdoAdapter $pdo) {
        $this->pdo = $pdo;
    }
}

$MyClass = $Dice->create('MyClass');
var_dump($pdo1 === $MyClass->pdo); //TRUE

// Echo string.
echo $Article->renderHelloWorld();

// Get one.
$result = $Article->fetchRow(array(
    ':article_id' => 11
));

print('fetch one: ');
var_dump($result);

// Get all.
$result = $Article->fetchRows(array());

print('fetch all: ');
var_dump(count($result));
var_dump($result);

// Create one.
$result = $Article->createRow(
    [
        ':title' => 'Hello World',
        ':description' => 'Hello World',
        ':content' => 'Hello World'
    ]
);

print('create: ');
var_dump($result);

// Get last inserted ID.
$article_id = $Article->InsertedId();

print('Last Inserted Id: ');
var_dump($article_id);

// Update one.
$result = $Article->updateRow(
    [
        ':title' => 'Hello World - updated',
        ':description' => 'Hello World - updated',
        ':content' => 'Hello World - updated',
        ':article_id' => $article_id
    ]
);

print('update: ');
var_dump($result);

// Delete one.
$result = $Article->deleteRow(
    [
        ':article_id' => $article_id
    ]
);

print('delete: ');
var_dump($result);
