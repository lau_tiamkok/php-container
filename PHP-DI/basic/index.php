<?php

require_once __DIR__ . '/vendor/autoload.php';

class Mailer
{
    public function mail($recipient, $content)
    {
        // send an email to the recipient
    }
}

class UserManager
{
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function register($email, $password)
    {
        // The user just registered, we create his account
        // ...

        // We send him an email to say hello!
        $this->mailer->mail($email, 'Hello and welcome!');
    }
}

$builder = new DI\ContainerBuilder();
$container = $builder->build();

// let PHP-DI figure out the dependencies.
// Behind the scenes, PHP-DI will create both a Mailer object and a UserManager object.
$userManager = $container->get('UserManager');
var_dump($userManager);
